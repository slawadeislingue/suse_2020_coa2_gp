using UnityEngine;

public class Countdown : MonoBehaviour
{
    /// <summary>
    /// Public value of the countdown
    /// </summary>
    public float countdown;

    public void Update()
    {
        // Time.deltaTime gives as the time it took to complete the very last frame
        // This is what we need to give us a "Realtime"-Countdown
        countdown -= Time.deltaTime;
    }
}