using UnityEngine;

public class FindPlayerByTag : MonoBehaviour
{
    /// <summary>
    /// The tag of the player - Set this in the inspector
    /// </summary>
    public string playerTag;
    
    /// <summary>
    /// The private reference to a GameObject called player
    /// </summary>
    private GameObject playerGO;
    
    
    private void Update()
    {
        // DONT DO THIS:
        FindTagHardcoded();
        
        // INSTEAD DO THIS
        FindTagByVariable();
        
        // EVEN BETTER DO THIS:
        FindTagByStaticVariable();
    }


    /// <summary>
    /// Finds the GameObject via it's tag you set in the Inspector of the GameObject
    /// The tag is hardcoded in this method
    /// </summary>
    public void FindTagHardcoded()
    {
        if (playerGO == null)
        {
            // DONT HARDCODE STRINGS
            playerGO = GameObject.FindWithTag("Player");
        }
    }

    /// <summary>
    /// Finds the GameObject via it's tag you set in the Inspector of the GameObject
    /// The tag is used via a variable we set in the Inspector
    /// </summary>
    public void FindTagByVariable()
    {
        if (playerGO == null)
        {
            // USE VARIABLES INSTEAD
            playerGO = GameObject.FindWithTag(playerTag);
        }
    }
    
    /// <summary>
    /// Finds the GameObject via it's tag you set in the Inspector of the GameObject
    /// The tag is set in a different class we can access directly
    /// </summary>
    private void FindTagByStaticVariable()
    {
        if (playerGO == null)
        {
            // USE STATIC CLASS WITH STATIC VARIABLES INSTEAD
            playerGO = GameObject.FindWithTag(GameTags.PLAYER);
        }
    }
}