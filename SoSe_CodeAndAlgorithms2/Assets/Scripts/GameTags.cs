/// <summary>
/// A static class where we directly access values without an instance of the class
/// This is used for the Tags of Unity and the only spot where we need to change the values of the Tags after
/// we set them in the TagManager of Unity
/// </summary>
public static class GameTags
{
    public static string PLAYER        = "Player";
    public static string GAMECONTOLLER = "GameController";
}