using UnityEngine;

public class SimplePlayerSpriteChanger : MonoBehaviour
{
    /// <summary>
    /// The public reference to the SpriteRenderer of the Player GameObject
    /// </summary>
    public SpriteRenderer playerSpriteRenderer;

    /// <summary>
    /// The public reference to the first Sprite in our Assets folder
    /// </summary>
    public Sprite firstSprite;

    /// <summary>
    /// The public reference to the second Sprite in our Assets folder
    /// </summary>
    public Sprite secondSprite;

    /// <summary>
    /// The public reference to the third Sprite in our Assets folder
    /// </summary>
    public Sprite thirdSprite;

    /// <summary>
    /// The OnClick-Callback Method set in the specific Button-OnClick List
    /// Sets the sprite of the playerSpriteRenderer to the first Sprite-Reference
    /// </summary>
    public void ChangeToFirstSprite()
    {
        playerSpriteRenderer.sprite = firstSprite;
    }

    /// <summary>
    /// The OnClick-Callback Method set in the specific Button-OnClick List
    /// Sets the sprite of the playerSpriteRenderer to the second Sprite-Reference
    /// </summary>
    public void ChangeToSecondSprite()
    {
        playerSpriteRenderer.sprite = secondSprite;
    }

    /// <summary>
    /// The OnClick-Callback Method set in the specific Button-OnClick List
    /// Sets the sprite of the playerSpriteRenderer to the third Sprite-Reference
    /// </summary>
    public void ChangeToThirdSprite()
    {
        playerSpriteRenderer.sprite = thirdSprite;
    }
}